/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/*
 * Public API Surface of angular-state
 */
export { StateService } from './lib/angular-state.service';
export { StateModule } from './lib/angular-state.module';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXItc3RhdGUvIiwic291cmNlcyI6WyJwdWJsaWNfYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQSw2QkFBYyw2QkFBNkIsQ0FBQztBQUM1Qyw0QkFBYyw0QkFBNEIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2YgYW5ndWxhci1zdGF0ZVxuICovXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL2FuZ3VsYXItc3RhdGUuc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9hbmd1bGFyLXN0YXRlLm1vZHVsZSc7XG4iXX0=