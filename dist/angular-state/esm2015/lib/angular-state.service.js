/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/index';
import * as i0 from "@angular/core";
export class StateService {
    /**
     * Service class constructor
     */
    constructor() {
        /**
                 * Do Nothing
                 */
        this._$states = new Map([]);
    }
    /**
     * Set an state property value
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    set(key, value) {
        if (this._$states.has(key)) {
            this._$states.get(key).next(value);
        }
        else {
            this._$states.set(key, new BehaviorSubject(value));
        }
        return this;
    }
    /**
     * Return the value of an specific state property
     * @param {?} key
     * @return {?}
     */
    get(key) {
        return !this._$states.has(key) ? null : this._$states.get(key).getValue();
    }
    /**
     * Getter for the node list attribute
     * @param {?} key
     * @return {?}
     */
    getObservable(key) {
        if (!this._$states.has(key)) {
            throw new Error(`App State property key "${key}" not found.`);
        }
        return this._$states.get(key).asObservable();
    }
}
StateService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
StateService.ctorParameters = () => [];
/** @nocollapse */ StateService.ngInjectableDef = i0.defineInjectable({ factory: function StateService_Factory() { return new StateService(); }, token: StateService, providedIn: "root" });
function StateService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    StateService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    StateService.ctorParameters;
    /** @type {?} */
    StateService.prototype._$states;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1zdGF0ZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1zdGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9hbmd1bGFyLXN0YXRlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFjLGVBQWUsRUFBRSxNQUFNLFlBQVksQ0FBQzs7QUFLekQsTUFBTTs7OztJQU9GOzs7O3dCQUwrRCxJQUFJLEdBQUcsQ0FBQyxFQUFFLENBQUM7S0FTekU7Ozs7Ozs7SUFLTSxHQUFHLENBQUMsR0FBVyxFQUFFLEtBQVU7UUFDOUIsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN0QztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUUsR0FBRyxFQUFFLElBQUksZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFFLENBQUM7U0FDeEQ7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDOzs7Ozs7O0lBTVQsR0FBRyxDQUFDLEdBQVc7UUFDbEIsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUUsR0FBRyxDQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7Ozs7Ozs7SUFNekUsYUFBYSxDQUFFLEdBQVc7UUFFN0IsRUFBRSxDQUFDLENBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDNUIsTUFBTSxJQUFJLEtBQUssQ0FBRSwyQkFBMkIsR0FBRyxjQUFjLENBQUUsQ0FBQztTQUNuRTtRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBRSxHQUFHLENBQUUsQ0FBQyxZQUFZLEVBQUUsQ0FBQzs7OztZQTVDdEQsVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcy9pbmRleCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgU3RhdGVTZXJ2aWNlIHtcblxuICAgIHByaXZhdGUgcmVhZG9ubHkgXyRzdGF0ZXM6IE1hcDxzdHJpbmcsIEJlaGF2aW9yU3ViamVjdDxhbnk+PiA9IG5ldyBNYXAoW10pO1xuXG4gICAgLyoqXG4gICAgICogU2VydmljZSBjbGFzcyBjb25zdHJ1Y3RvclxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICAvKipcbiAgICAgICAgICogRG8gTm90aGluZ1xuICAgICAgICAgKi9cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXQgYW4gc3RhdGUgcHJvcGVydHkgdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0KGtleTogc3RyaW5nLCB2YWx1ZTogYW55KTogU3RhdGVTZXJ2aWNlIHtcbiAgICAgICAgaWYgKCB0aGlzLl8kc3RhdGVzLmhhcyhrZXkpICkge1xuICAgICAgICAgICAgdGhpcy5fJHN0YXRlcy5nZXQoa2V5KS5uZXh0KHZhbHVlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuXyRzdGF0ZXMuc2V0KCBrZXksIG5ldyBCZWhhdmlvclN1YmplY3QodmFsdWUpICk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJuIHRoZSB2YWx1ZSBvZiBhbiBzcGVjaWZpYyBzdGF0ZSBwcm9wZXJ0eVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQoa2V5OiBzdHJpbmcpOiBhbnkge1xuICAgICAgICByZXR1cm4gIXRoaXMuXyRzdGF0ZXMuaGFzKGtleSkgPyBudWxsIDogdGhpcy5fJHN0YXRlcy5nZXQoIGtleSApLmdldFZhbHVlKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGZvciB0aGUgbm9kZSBsaXN0IGF0dHJpYnV0ZVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXRPYnNlcnZhYmxlKCBrZXk6IHN0cmluZyApOiBPYnNlcnZhYmxlPGFueT4ge1xuXG4gICAgICAgIGlmICggIXRoaXMuXyRzdGF0ZXMuaGFzKGtleSkgKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoIGBBcHAgU3RhdGUgcHJvcGVydHkga2V5IFwiJHtrZXl9XCIgbm90IGZvdW5kLmAgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLl8kc3RhdGVzLmdldCgga2V5ICkuYXNPYnNlcnZhYmxlKCk7XG4gICAgfVxuXG59XG4iXX0=