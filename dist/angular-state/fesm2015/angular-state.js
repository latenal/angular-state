import { Injectable, NgModule, defineInjectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/index';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class StateService {
    /**
     * Service class constructor
     */
    constructor() {
        /**
                 * Do Nothing
                 */
        this._$states = new Map([]);
    }
    /**
     * Set an state property value
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    set(key, value) {
        if (this._$states.has(key)) {
            this._$states.get(key).next(value);
        }
        else {
            this._$states.set(key, new BehaviorSubject(value));
        }
        return this;
    }
    /**
     * Return the value of an specific state property
     * @param {?} key
     * @return {?}
     */
    get(key) {
        return !this._$states.has(key) ? null : this._$states.get(key).getValue();
    }
    /**
     * Getter for the node list attribute
     * @param {?} key
     * @return {?}
     */
    getObservable(key) {
        if (!this._$states.has(key)) {
            throw new Error(`App State property key "${key}" not found.`);
        }
        return this._$states.get(key).asObservable();
    }
}
StateService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
StateService.ctorParameters = () => [];
/** @nocollapse */ StateService.ngInjectableDef = defineInjectable({ factory: function StateService_Factory() { return new StateService(); }, token: StateService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class StateModule {
}
StateModule.decorators = [
    { type: NgModule, args: [{
                providers: [StateService]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { StateService, StateModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1zdGF0ZS5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vYW5ndWxhci1zdGF0ZS9saWIvYW5ndWxhci1zdGF0ZS5zZXJ2aWNlLnRzIiwibmc6Ly9hbmd1bGFyLXN0YXRlL2xpYi9hbmd1bGFyLXN0YXRlLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzL2luZGV4JztcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBTdGF0ZVNlcnZpY2Uge1xuXG4gICAgcHJpdmF0ZSByZWFkb25seSBfJHN0YXRlczogTWFwPHN0cmluZywgQmVoYXZpb3JTdWJqZWN0PGFueT4+ID0gbmV3IE1hcChbXSk7XG5cbiAgICAvKipcbiAgICAgKiBTZXJ2aWNlIGNsYXNzIGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBEbyBOb3RoaW5nXG4gICAgICAgICAqL1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldCBhbiBzdGF0ZSBwcm9wZXJ0eSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQoa2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpOiBTdGF0ZVNlcnZpY2Uge1xuICAgICAgICBpZiAoIHRoaXMuXyRzdGF0ZXMuaGFzKGtleSkgKSB7XG4gICAgICAgICAgICB0aGlzLl8kc3RhdGVzLmdldChrZXkpLm5leHQodmFsdWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5fJHN0YXRlcy5zZXQoIGtleSwgbmV3IEJlaGF2aW9yU3ViamVjdCh2YWx1ZSkgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIHZhbHVlIG9mIGFuIHNwZWNpZmljIHN0YXRlIHByb3BlcnR5XG4gICAgICovXG4gICAgcHVibGljIGdldChrZXk6IHN0cmluZyk6IGFueSB7XG4gICAgICAgIHJldHVybiAhdGhpcy5fJHN0YXRlcy5oYXMoa2V5KSA/IG51bGwgOiB0aGlzLl8kc3RhdGVzLmdldCgga2V5ICkuZ2V0VmFsdWUoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgZm9yIHRoZSBub2RlIGxpc3QgYXR0cmlidXRlXG4gICAgICovXG4gICAgcHVibGljIGdldE9ic2VydmFibGUoIGtleTogc3RyaW5nICk6IE9ic2VydmFibGU8YW55PiB7XG5cbiAgICAgICAgaWYgKCAhdGhpcy5fJHN0YXRlcy5oYXMoa2V5KSApIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvciggYEFwcCBTdGF0ZSBwcm9wZXJ0eSBrZXkgXCIke2tleX1cIiBub3QgZm91bmQuYCApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuXyRzdGF0ZXMuZ2V0KCBrZXkgKS5hc09ic2VydmFibGUoKTtcbiAgICB9XG5cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTdGF0ZVNlcnZpY2UgfSBmcm9tICcuL2FuZ3VsYXItc3RhdGUuc2VydmljZSc7XG5cbkBOZ01vZHVsZSh7XG4gIHByb3ZpZGVyczogWyBTdGF0ZVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBTdGF0ZU1vZHVsZSB7IH1cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7SUFhSTs7Ozt3QkFMK0QsSUFBSSxHQUFHLENBQUMsRUFBRSxDQUFDO0tBU3pFOzs7Ozs7O0lBS00sR0FBRyxDQUFDLEdBQVcsRUFBRSxLQUFVO1FBQzlCLElBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFFLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3RDO2FBQU07WUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBRSxHQUFHLEVBQUUsSUFBSSxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUUsQ0FBQztTQUN4RDtRQUNELE9BQU8sSUFBSSxDQUFDOzs7Ozs7O0lBTVQsR0FBRyxDQUFDLEdBQVc7UUFDbEIsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBRSxHQUFHLENBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs7Ozs7OztJQU16RSxhQUFhLENBQUUsR0FBVztRQUU3QixJQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFFLEVBQUU7WUFDM0IsTUFBTSxJQUFJLEtBQUssQ0FBRSwyQkFBMkIsR0FBRyxjQUFjLENBQUUsQ0FBQztTQUNuRTtRQUVELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUUsR0FBRyxDQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7Ozs7WUE1Q3RELFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7Ozs7Ozs7OztBQ0xEOzs7WUFHQyxRQUFRLFNBQUM7Z0JBQ1IsU0FBUyxFQUFFLENBQUUsWUFBWSxDQUFFO2FBQzVCOzs7Ozs7Ozs7Ozs7Ozs7In0=