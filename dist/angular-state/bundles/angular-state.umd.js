(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs/index')) :
    typeof define === 'function' && define.amd ? define('angular-state', ['exports', '@angular/core', 'rxjs/index'], factory) :
    (factory((global['angular-state'] = {}),global.ng.core,global.rxjs.index));
}(this, (function (exports,i0,index) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var StateService = (function () {
        /**
         * Service class constructor
         */
        function StateService() {
            /**
                     * Do Nothing
                     */
            this._$states = new Map([]);
        }
        /**
         * Set an state property value
         * @param {?} key
         * @param {?} value
         * @return {?}
         */
        StateService.prototype.set = /**
         * Set an state property value
         * @param {?} key
         * @param {?} value
         * @return {?}
         */
            function (key, value) {
                if (this._$states.has(key)) {
                    this._$states.get(key).next(value);
                }
                else {
                    this._$states.set(key, new index.BehaviorSubject(value));
                }
                return this;
            };
        /**
         * Return the value of an specific state property
         * @param {?} key
         * @return {?}
         */
        StateService.prototype.get = /**
         * Return the value of an specific state property
         * @param {?} key
         * @return {?}
         */
            function (key) {
                return !this._$states.has(key) ? null : this._$states.get(key).getValue();
            };
        /**
         * Getter for the node list attribute
         * @param {?} key
         * @return {?}
         */
        StateService.prototype.getObservable = /**
         * Getter for the node list attribute
         * @param {?} key
         * @return {?}
         */
            function (key) {
                if (!this._$states.has(key)) {
                    throw new Error("App State property key \"" + key + "\" not found.");
                }
                return this._$states.get(key).asObservable();
            };
        StateService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        StateService.ctorParameters = function () { return []; };
        /** @nocollapse */ StateService.ngInjectableDef = i0.defineInjectable({ factory: function StateService_Factory() { return new StateService(); }, token: StateService, providedIn: "root" });
        return StateService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var StateModule = (function () {
        function StateModule() {
        }
        StateModule.decorators = [
            { type: i0.NgModule, args: [{
                        providers: [StateService]
                    },] },
        ];
        return StateModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.StateService = StateService;
    exports.StateModule = StateModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1zdGF0ZS51bWQuanMubWFwIiwic291cmNlcyI6WyJuZzovL2FuZ3VsYXItc3RhdGUvbGliL2FuZ3VsYXItc3RhdGUuc2VydmljZS50cyIsIm5nOi8vYW5ndWxhci1zdGF0ZS9saWIvYW5ndWxhci1zdGF0ZS5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcy9pbmRleCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgU3RhdGVTZXJ2aWNlIHtcblxuICAgIHByaXZhdGUgcmVhZG9ubHkgXyRzdGF0ZXM6IE1hcDxzdHJpbmcsIEJlaGF2aW9yU3ViamVjdDxhbnk+PiA9IG5ldyBNYXAoW10pO1xuXG4gICAgLyoqXG4gICAgICogU2VydmljZSBjbGFzcyBjb25zdHJ1Y3RvclxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICAvKipcbiAgICAgICAgICogRG8gTm90aGluZ1xuICAgICAgICAgKi9cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXQgYW4gc3RhdGUgcHJvcGVydHkgdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0KGtleTogc3RyaW5nLCB2YWx1ZTogYW55KTogU3RhdGVTZXJ2aWNlIHtcbiAgICAgICAgaWYgKCB0aGlzLl8kc3RhdGVzLmhhcyhrZXkpICkge1xuICAgICAgICAgICAgdGhpcy5fJHN0YXRlcy5nZXQoa2V5KS5uZXh0KHZhbHVlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuXyRzdGF0ZXMuc2V0KCBrZXksIG5ldyBCZWhhdmlvclN1YmplY3QodmFsdWUpICk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJuIHRoZSB2YWx1ZSBvZiBhbiBzcGVjaWZpYyBzdGF0ZSBwcm9wZXJ0eVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQoa2V5OiBzdHJpbmcpOiBhbnkge1xuICAgICAgICByZXR1cm4gIXRoaXMuXyRzdGF0ZXMuaGFzKGtleSkgPyBudWxsIDogdGhpcy5fJHN0YXRlcy5nZXQoIGtleSApLmdldFZhbHVlKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGZvciB0aGUgbm9kZSBsaXN0IGF0dHJpYnV0ZVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXRPYnNlcnZhYmxlKCBrZXk6IHN0cmluZyApOiBPYnNlcnZhYmxlPGFueT4ge1xuXG4gICAgICAgIGlmICggIXRoaXMuXyRzdGF0ZXMuaGFzKGtleSkgKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoIGBBcHAgU3RhdGUgcHJvcGVydHkga2V5IFwiJHtrZXl9XCIgbm90IGZvdW5kLmAgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLl8kc3RhdGVzLmdldCgga2V5ICkuYXNPYnNlcnZhYmxlKCk7XG4gICAgfVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3RhdGVTZXJ2aWNlIH0gZnJvbSAnLi9hbmd1bGFyLXN0YXRlLnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBwcm92aWRlcnM6IFsgU3RhdGVTZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgU3RhdGVNb2R1bGUgeyB9XG4iXSwibmFtZXMiOlsiQmVoYXZpb3JTdWJqZWN0IiwiSW5qZWN0YWJsZSIsIk5nTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7Ozs7UUFhSTs7Ozs0QkFMK0QsSUFBSSxHQUFHLENBQUMsRUFBRSxDQUFDO1NBU3pFOzs7Ozs7O1FBS00sMEJBQUc7Ozs7OztzQkFBQyxHQUFXLEVBQUUsS0FBVTtnQkFDOUIsSUFBSyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUUsRUFBRTtvQkFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN0QztxQkFBTTtvQkFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBRSxHQUFHLEVBQUUsSUFBSUEscUJBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBRSxDQUFDO2lCQUN4RDtnQkFDRCxPQUFPLElBQUksQ0FBQzs7Ozs7OztRQU1ULDBCQUFHOzs7OztzQkFBQyxHQUFXO2dCQUNsQixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFFLEdBQUcsQ0FBRSxDQUFDLFFBQVEsRUFBRSxDQUFDOzs7Ozs7O1FBTXpFLG9DQUFhOzs7OztzQkFBRSxHQUFXO2dCQUU3QixJQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFFLEVBQUU7b0JBQzNCLE1BQU0sSUFBSSxLQUFLLENBQUUsOEJBQTJCLEdBQUcsa0JBQWMsQ0FBRSxDQUFDO2lCQUNuRTtnQkFFRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFFLEdBQUcsQ0FBRSxDQUFDLFlBQVksRUFBRSxDQUFDOzs7b0JBNUN0REMsYUFBVSxTQUFDO3dCQUNSLFVBQVUsRUFBRSxNQUFNO3FCQUNyQjs7Ozs7MkJBTEQ7Ozs7Ozs7QUNBQTs7OztvQkFHQ0MsV0FBUSxTQUFDO3dCQUNSLFNBQVMsRUFBRSxDQUFFLFlBQVksQ0FBRTtxQkFDNUI7OzBCQUxEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=