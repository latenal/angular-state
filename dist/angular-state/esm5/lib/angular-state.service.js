/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/index';
import * as i0 from "@angular/core";
var StateService = /** @class */ (function () {
    /**
     * Service class constructor
     */
    function StateService() {
        /**
                 * Do Nothing
                 */
        this._$states = new Map([]);
    }
    /**
     * Set an state property value
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    StateService.prototype.set = /**
     * Set an state property value
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    function (key, value) {
        if (this._$states.has(key)) {
            this._$states.get(key).next(value);
        }
        else {
            this._$states.set(key, new BehaviorSubject(value));
        }
        return this;
    };
    /**
     * Return the value of an specific state property
     * @param {?} key
     * @return {?}
     */
    StateService.prototype.get = /**
     * Return the value of an specific state property
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return !this._$states.has(key) ? null : this._$states.get(key).getValue();
    };
    /**
     * Getter for the node list attribute
     * @param {?} key
     * @return {?}
     */
    StateService.prototype.getObservable = /**
     * Getter for the node list attribute
     * @param {?} key
     * @return {?}
     */
    function (key) {
        if (!this._$states.has(key)) {
            throw new Error("App State property key \"" + key + "\" not found.");
        }
        return this._$states.get(key).asObservable();
    };
    StateService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    StateService.ctorParameters = function () { return []; };
    /** @nocollapse */ StateService.ngInjectableDef = i0.defineInjectable({ factory: function StateService_Factory() { return new StateService(); }, token: StateService, providedIn: "root" });
    return StateService;
}());
export { StateService };
function StateService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    StateService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    StateService.ctorParameters;
    /** @type {?} */
    StateService.prototype._$states;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1zdGF0ZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1zdGF0ZS8iLCJzb3VyY2VzIjpbImxpYi9hbmd1bGFyLXN0YXRlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFjLGVBQWUsRUFBRSxNQUFNLFlBQVksQ0FBQzs7O0lBU3JEOztPQUVHO0lBQ0g7Ozs7d0JBTCtELElBQUksR0FBRyxDQUFDLEVBQUUsQ0FBQztLQVN6RTs7Ozs7OztJQUtNLDBCQUFHOzs7Ozs7Y0FBQyxHQUFXLEVBQUUsS0FBVTtRQUM5QixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3RDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBRSxHQUFHLEVBQUUsSUFBSSxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUUsQ0FBQztTQUN4RDtRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7Ozs7Ozs7SUFNVCwwQkFBRzs7Ozs7Y0FBQyxHQUFXO1FBQ2xCLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFFLEdBQUcsQ0FBRSxDQUFDLFFBQVEsRUFBRSxDQUFDOzs7Ozs7O0lBTXpFLG9DQUFhOzs7OztjQUFFLEdBQVc7UUFFN0IsRUFBRSxDQUFDLENBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDNUIsTUFBTSxJQUFJLEtBQUssQ0FBRSw4QkFBMkIsR0FBRyxrQkFBYyxDQUFFLENBQUM7U0FDbkU7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUUsR0FBRyxDQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7OztnQkE1Q3RELFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7O3VCQUxEOztTQU1hLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzL2luZGV4JztcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBTdGF0ZVNlcnZpY2Uge1xuXG4gICAgcHJpdmF0ZSByZWFkb25seSBfJHN0YXRlczogTWFwPHN0cmluZywgQmVoYXZpb3JTdWJqZWN0PGFueT4+ID0gbmV3IE1hcChbXSk7XG5cbiAgICAvKipcbiAgICAgKiBTZXJ2aWNlIGNsYXNzIGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBEbyBOb3RoaW5nXG4gICAgICAgICAqL1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldCBhbiBzdGF0ZSBwcm9wZXJ0eSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQoa2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpOiBTdGF0ZVNlcnZpY2Uge1xuICAgICAgICBpZiAoIHRoaXMuXyRzdGF0ZXMuaGFzKGtleSkgKSB7XG4gICAgICAgICAgICB0aGlzLl8kc3RhdGVzLmdldChrZXkpLm5leHQodmFsdWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5fJHN0YXRlcy5zZXQoIGtleSwgbmV3IEJlaGF2aW9yU3ViamVjdCh2YWx1ZSkgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIHZhbHVlIG9mIGFuIHNwZWNpZmljIHN0YXRlIHByb3BlcnR5XG4gICAgICovXG4gICAgcHVibGljIGdldChrZXk6IHN0cmluZyk6IGFueSB7XG4gICAgICAgIHJldHVybiAhdGhpcy5fJHN0YXRlcy5oYXMoa2V5KSA/IG51bGwgOiB0aGlzLl8kc3RhdGVzLmdldCgga2V5ICkuZ2V0VmFsdWUoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgZm9yIHRoZSBub2RlIGxpc3QgYXR0cmlidXRlXG4gICAgICovXG4gICAgcHVibGljIGdldE9ic2VydmFibGUoIGtleTogc3RyaW5nICk6IE9ic2VydmFibGU8YW55PiB7XG5cbiAgICAgICAgaWYgKCAhdGhpcy5fJHN0YXRlcy5oYXMoa2V5KSApIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvciggYEFwcCBTdGF0ZSBwcm9wZXJ0eSBrZXkgXCIke2tleX1cIiBub3QgZm91bmQuYCApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuXyRzdGF0ZXMuZ2V0KCBrZXkgKS5hc09ic2VydmFibGUoKTtcbiAgICB9XG5cbn1cbiJdfQ==