import { Observable } from 'rxjs/index';
export declare class StateService {
    private readonly _$states;
    /**
     * Service class constructor
     */
    constructor();
    /**
     * Set an state property value
     */
    set(key: string, value: any): StateService;
    /**
     * Return the value of an specific state property
     */
    get(key: string): any;
    /**
     * Getter for the node list attribute
     */
    getObservable(key: string): Observable<any>;
}
