/*
 * Public API Surface of angular-state
 */

export * from './lib/angular-state.service';
export * from './lib/angular-state.module';
