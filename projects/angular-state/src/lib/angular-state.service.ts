import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs/index';

@Injectable({
    providedIn: 'root'
})
export class StateService {

    private readonly _$states: Map<string, BehaviorSubject<any>> = new Map([]);

    /**
     * Service class constructor
     */
    constructor() {
        /**
         * Do Nothing
         */
    }

    /**
     * Set an state property value
     */
    public set(key: string, value: any): StateService {
        if ( this._$states.has(key) ) {
            this._$states.get(key).next(value);
        } else {
            this._$states.set( key, new BehaviorSubject(value) );
        }
        return this;
    }

    /**
     * Return the value of an specific state property
     */
    public get(key: string): any {
        return !this._$states.has(key) ? null : this._$states.get( key ).getValue();
    }

    /**
     * Getter for the node list attribute
     */
    public getObservable( key: string ): Observable<any> {

        if ( !this._$states.has(key) ) {
            throw new Error( `App State property key "${key}" not found.` );
        }

        return this._$states.get( key ).asObservable();
    }

}
