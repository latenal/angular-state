import { NgModule } from '@angular/core';
import { StateService } from './angular-state.service';

@NgModule({
  providers: [ StateService ]
})
export class StateModule { }
